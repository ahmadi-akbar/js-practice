const users = [
  {
    firstName: "John",
    lastName: "Smith",
    phoneNumber: 123456789,
    accounts: {
      ing: "NL69INGB0123456789",
      rabobank: "NL44RABO0123456789",
      abnamro: "NL02ABNA0123456789",
    },
  },
  {
    firstName: "Josh",
    lastName: "Cruz",
    phoneNumber: 987654321,
    accounts: {
      ing: "NL71INGB0123456789",
      snsbank: "NL12SNSB0123456789",
    },
  },
];

const PrintName = (user) => {
  return user.firstName + " " + user.lastName;
};

const PrintName2 = ({ firstName, lastName, ...rest }) => {
  console.log("rest", rest);
  return firstName + " " + lastName;
};
console.log(PrintName2(users[0]));

// console.log(PrintName(users[1]));
