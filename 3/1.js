const persons = [
  {
    firstName: "John",
    lastName: "Smith",
    phoneNumber: 123456789,
    accounts: {
      ing: "NL69INGB0123456789",
      rabobank: "NL44RABO0123456789",
      abnamro: "NL02ABNA0123456789",
    },
  },
  {
    firstName: "Josh",
    lastName: "Cruz",
    phoneNumber: 987654321,
    accounts: {
      ing: "NL71INGB0123456789",
      snsbank: "NL12SNSB0123456789",
    },
  },
];

const [{ firstName: name, accounts }, { firstName: name2 }] = persons;

const { ing } = accounts;
// equal
// const name = persons[0].firstName;

console.log(name);
console.log(ing);
