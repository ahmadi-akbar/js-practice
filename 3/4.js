const User1 = {
  firstName: "John",
  lastName: "Smith",
  phoneNumber: 123456789,
  accounts: {
    ing: "NL69INGB0123456789",
    rabobank: "NL44RABO0123456789",
    abnamro: "NL02ABNA0123456789",
  },
};
const User2 = {
  firstName: "Josh",
  lastName: "Cruz",
  phoneNumber: 987654321,
  accounts: {
    ing: "NL71INGB0123456789",
    snsbank: "NL12SNSB0123456789",
  },
};

const userDetail1 = {
  email: "a@a.com",
  sex: "man",
};
const userDetail2 = {
  email: "b@a.com",
  sex: "woman",
};

const PrintName = (user, detail) =>
  detail.email + " : " + user.firstName + " " + user.lastName + " ";
// equal
const PrintName2 = (user, detail) => {
  const { firstName, lastName, ...rest } = user;
  const { email } = detail;
  return email + " : " + firstName + " " + lastName;
};
// equal
const PrintName3 = ({ firstName, lastName, ...rest }, { email }) =>
  email + " : " + firstName + " " + lastName;

console.log(PrintName(User1, userDetail1));
console.log(PrintName2(User1, userDetail1));
console.log(PrintName3(User1, userDetail1));
