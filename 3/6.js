const result = {
  success: ["max-length", "no-amd", "prefer-arrow-functions"],
  failure: ["no-var", "var-on-top", "linebreak", "test", "test2"],
  skipped: ["no-extra-semi", "no-dup-keys"],
};

function makeList(arr) {
  //   const failureItems = [
  //     `<li class="text-warning">${arr[0]}</li>`,
  //     `<li class="text-warning">${arr[1]}</li>`,
  //     `<li class="text-warning">${arr[2]}</li>`,
  //   ];

  //   const failureItems = arr.map((i) => {
  //     return `<li class="text-warning">${i}</li>`;
  //   });
  //equal

  const failureItems = arr.map((i) => `<li class="text-warning">${i}</li>`);
  return failureItems;
  // equal
  //   return arr.map((i) => `<li class="text-warning">${i}</li>`);
}

//equal
const makeList2 = (arr) => arr.map((i) => `<li class="text-warning">${i}</li>`);

const failuresList = makeList(result.failure);
const failuresList2 = makeList2(result.failure);

console.log(failuresList);
console.log(failuresList2);
