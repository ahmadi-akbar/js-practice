let obj = {
  name: "FreeCodeCamp",
  review: "Awesome",
};

// Object.freeze(obj);



obj.review = "bad"; // will be ignored. Mutation not allowed
obj.newProp = "Test"; // will be ignored. Mutation not allowed


let k = Object.values(obj);
console.log(obj);
console.log("k:", k);
// { name: "FreeCodeCamp", review:"Awesome"}
