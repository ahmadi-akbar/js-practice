// "use strict";

// a = "aaaa";

// console.log("var a:", a);

function fun() {
  let a = "scoped a ";
  console.log("var a in fun:", a);
}

fun();

console.log("var a out of fun:", a);
