const myFunc = function () {
  const myVar = "value";
  return myVar;
};

const myFunc2 = function () {
  return "value";
};

const myFunc3 = () => {
  return "value";
};

const myFunc4 = () => "value";

let ret = myFunc3();

console.log("ret", ret);
