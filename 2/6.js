// Destructuring
const user = {
  name: "John Doe",
  age: 34,
  prop: { low: 1, high: 10 },
  prop2: 2,
};

const {
  name: UserName,
  age: UserAge, // rename
  prop: { low, high }, //nested
  email = "example@a.com", // default value
} = user;

console.log("user", user);

console.log("user email", email);

// const prop = user.prop

// const { low, high } = prop;
// console.log("prop:", prop);
console.log("prop low:", low);
console.log("prop high:", high);
