const sum = (a, b) => a + b;

const print = (str = "null string") => console.log(str);

// without parameter
print();

// with parameter
print("some longgg string");

console.log("sum :", sum(10));
