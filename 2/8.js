const arr = [1, 2, 3, 4, 5, 6];
const [a1, b1, ...c1] = arr;
console.log(a1, b1, c1); // 1 2 [ 3, 4, 5, 6 ]

// Array.prototype.slice()
console.log("slice => ", arr.slice(2, 4));
