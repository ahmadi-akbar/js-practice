const user = { name: "John Doe", age: 34, prop: 1, prop2: 2 };

const { name: UserName, age: UserAge, newProp } = user;

// ==========
// const name = user.name; // name = 'John Doe'
// const age = user.age; // age = 34

console.log("user", user);

console.log("name:", UserName);

console.log("age:", UserAge);

console.log("newProp:", newProp);

// name = 'John Doe', age = 34
