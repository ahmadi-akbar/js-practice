const sum = (...arr) => {
  let total = 0;
  let temp = arr.map((num) => (total += num));
  console.log("temp", temp);
  return total;
};

const sum2 = (...args) => {
  let a = args.reduce((a, b) => a + b, 0);
  console.log("a", a);
};

console.log("sum => ", sum(1, 5, 6, 14, 65, 7, 8, 9, 6));
